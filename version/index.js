const simpleGit = require( "simple-git" );
const util = require( "util" );
const fs = require( "fs" );
const readFile = util.promisify( fs.readFile );

const git = simpleGit();

const mergeTypeRE = /[\s\S]*?Merged?(?: branch)?(?: in)? '?(feature|bugfix|task)\/[\s\S]*/;

/**
 * Get the line-number on which the 'version' of the project is declared.
 *
 * @param {Buffer} fileContent The contents of package.json
 * @returns {int} The line-number on which version is declared
 */
function getVersionLine( fileContent = new Buffer( "" ) ) {
    return fileContent
        // JS is better with Strings than Buffers ;)
        .toString()
        // Stop bothering to read after "version"
        .slice( 0, fileContent.indexOf( '"version"' ) )
        // Divide by newlines
        .split( "\n" )
        // Count what line version is on
        .length
    ;
}

/**
 * Get the commit-SHA of the last time the version was updated in the package.json.
 *
 * @param {Buffer} fileContent The contents of package.json
 * @returns {Promise<String>} Promise that resolves to a String containing the commit-SHA
 */
async function getCommitSHA( fileContent = new Buffer( "" ) ) {
    const versionLine = getVersionLine( fileContent );
    return (
            await git.raw( "blame", "-L", `${
                versionLine
            },${
                versionLine
            }`, "package.json" )
        )
        //Only interrested in SHA, drop the rest
        .split( " " )[0]
        // Take care of rare (early) commits with short SHA
        .replace( "^", "" )
    ;
}

/**
 * Get all merge-commits since version was last updated in 'package.json'.
 * Additionally map the full commit-messages to only 'feature|bugfix'.
 *
 * @param {Buffer} fileContent The contents of 'package.json'
 * @returns {Promise<String[]>} A `Promise` that resolved to an Array of `String`s
 */
async function getMergeCommits( fileContent = new Buffer( "" ) ) {
    const commitSHA = await getCommitSHA( fileContent );
    const commits = await git.raw( "log", "--merges", `${commitSHA}..HEAD` );
    // Extract type of merge commits (feature / bugfix)
    const shortHand = commits.split( "Merge:" ).filter(
        ( value ) => mergeTypeRE.test( value ),
    ).map(
        ( value ) => value.replace( mergeTypeRE, "$1" ),
    );
    return shortHand;
}

/**
 * Get the version as an array of 'major', 'minor' and 'patch' from the 'package.json'.
 * Additionally maps the values to number.
 *
 * @param {Buffer} fileContent The contents of 'package.json'
 * @returns {int[]} Array of ints indication 'major', 'minor' and 'patch' version
 */
function getVersionFromPackage( fileContent = new Buffer( "" ) ) {
    const npmPackage = JSON.parse( fileContent );
    return npmPackage.version.split( "." ).map(
        ( n ) => Number( n ),
    );
}

/**
 * Calculate semver based on merge-commits. All 'feature' commits up the minor
 * version.
 * Any other merge-commit after the last 'feature' will up the patch (starting from 0)
 * 
 * @param {int[]} baseVersion Array of three `int`s (major, minor, patch)
 * @param {String[]} mergeCommits Array of Strings for each merge-commit
 */
function calculateVersion(
    baseVersion = [ 1,0,0 ],
    mergeCommits = [
        "bugfix",
    ],
) {
    return mergeCommits.reduce(
        ( acc, v ) => {
          if ( v === "feature" ) {
            return [
              acc[0],
              ++acc[1],
              0,
            ];
          }
          return [
            acc[0],
            acc[1],
            ++acc[2],
          ]
        },
        baseVersion,
    ).join( "." );
}

module.exports = async function getVersion() {
    const packageJSON = await readFile( "./package.json" );
    const baseVersion = getVersionFromPackage( packageJSON );
    const mergeCommits = await getMergeCommits( packageJSON );
    return calculateVersion( baseVersion, mergeCommits );
}
