#!/usr/bin/env node

const getVersion = require('../version');

getVersion()
    .then((version) => {
        console.log(version);
    })
;