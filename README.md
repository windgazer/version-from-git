# Version From Git

Calculate project-version, adding up all matching merge-commits since `version` was last changed in `package.json`. All `feature` merges are counted towards minor versions, and since last feature all bugfix merges are counted towards `patch`.

## Install

> `npm i -g version-from-git`

## Use

When in the root of an nodejs package, which is also the git-root, simply run the following command:
> `version-from-git`

### Credits

- [developer.okta.com/blog/2019/06/18/command-line-app-with-nodejs](https://developer.okta.com/blog/2019/06/18/command-line-app-with-nodejs)